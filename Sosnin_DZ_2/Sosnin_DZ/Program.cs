﻿//Sosnin F2 IU6-76B P2
using System;
using System.Text.Json;
using System.Text.Json.Serialization;


namespace Sosnin_DZ
{
    public class Compositor
    {
        public string Name { get; set; }
        public string Surname {get; set;}

        /*public Compositor() //конструктор по умолчанию
        {
            Name = "Неизвестно";
            Surname = "Неизвестно";
            Console.WriteLine("Имя: {0}\nФамилия: {1}\n", Name, Surname);
        }
        public Compositor(string name,string surname) //конструктор по умолчанию
        {
            Name = name;
            Surname = surname;
            Console.WriteLine("Имя: {0}\nФамилия: {1}\n", Name, Surname);
        }
        public string Represent()
        {
            string author = Name + ' ' + Surname;
            return author;
        }*/
    }
    public class Album
    {
        public string Name { get; set; }
        public int Year { get; set; }

        /*public Album() //конструктор по умолчанию
        {
            Name = "Неизвестно";
            Year =  0;
            Console.WriteLine("Название: {0}\nГод: {1}\n", Name, Year);
        }
        public Album(string name, int year)
        {
            Name = name;
            Year = year;
            Console.WriteLine("Название: {0}\nГод: {1}\n", Name, year);
        }
        public string Represent()
        {
            string album = Name + " Год: " + Year.ToString();
            return album;
        }*/
    }
    public class Song
    {
        public string Name { get; set; }
        public string Gener { get; set; }
        public int Year { get; set; }
        public Compositor[] Authors { get; set; }
        //public Compositor Author1, Author2, Author;
        public Album Album_s { get; set; }
        /*public Song() //конструктор по умолчанию
        {
            Name = "Неизвестно";
            Gener = "Неизвестно";
            Year = 0;
            Console.WriteLine("Название: {0}\nЖанр: {1}\nГод: {2}\n", Name, Gener,"Неизвесто");
        }
        public Song(string name,string gener, int year) //нет композитора
        {
            Name = name;
            Gener = gener;
            Year = year;
            Console.WriteLine("Название: {0}\nЖанр: {1}\nГод: {2}\n", Name, Gener, Year);
        }
        public Song(string name, string gener, int year, Compositor author) //композитор
        {
            Name = name;
            Gener = gener;
            Year = year;
            Author = author;
            Console.WriteLine("Название: {0}\nЖанр: {1}\nГод: {2}\nАвтор: {3}\n", Name, Gener, Year, Author.Represent());

      }
        public Song(string name, string gener, int year, Compositor author1, Compositor author2)//два композитора
        {
            Name = name;
            Gener = gener;
            Year = year;
            Author1 = author1;
            Author2 = author2;
            Console.WriteLine("Название: {0}\nЖанр: {1}\nГод: {2}\nАвторs: {3}, {4}\n", Name, Gener, Year, Author1.Represent(), Author2.Represent());

        }
        public Song(string name, string gener, int year, Compositor author, Compositor author1, Compositor author2)//три композитора
        {
            Name = name;
            Gener = gener;
            Year = year;
            Author = author;
            Author1 = author1;
            Author2 = author2;
            Console.WriteLine("Название: {0}\nЖанр: {1}\nГод: {2}\nАвторs: {3}, {4}, {5}\n", Name, Gener, Year, Author.Represent(), Author1.Represent(), Author2.Represent());

        }
        //с албомом
        public Song(string name, string gener, int year,Album album) //нет композитора
        {
            Name = name;
            Gener = gener;
            Year = year;
            Album_s = album;
            Console.WriteLine("Название: {0}\nЖанр: {1}\nГод: {2}\nАльбом: {3}\n", Name, Gener, Year, Album_s.Represent()); ;
        }
        public Song(string name, string gener, int year, Compositor author, Album album) //композитор
        {
            Name = name;
            Gener = gener;
            Year = year;
            Author = author;
            Album_s = album;
            Console.WriteLine("Название: {0}\nЖанр: {1}\nГод: {2}\nАвтор: {3}\nАльбом: {4}\n", Name, Gener, Year, Author.Represent(), Album_s.Represent());

        }
        public Song(string name, string gener, int year, Compositor author1, Compositor author2, Album album)//два композитора
        {
            Name = name;
            Gener = gener;
            Year = year;
            Author1 = author1;
            Author2 = author2;
            Album_s = album;
            Console.WriteLine("Название: {0}\nЖанр: {1}\nГод: {2}\nАвторs: {3}, {4}\nАльбом: {5}\n", Name, Gener, Year, Author1.Represent(), Author2.Represent(), Album_s.Represent());

        }
        public Song(string name, string gener, int year, Compositor author, Compositor author1, Compositor author2, Album album)//три композитора
        {
            Name = name;
            Gener = gener;
            Year = year;
            Author = author;
            Author1 = author1;
            Author2 = author2;
            Album_s = album;
            Console.WriteLine("Название: {0}\nЖанр: {1}\nГод: {2}\nАвторs: {3}, {4}, {5}\nАльбом: {6}\n", Name, Gener, Year, Author.Represent(), Author1.Represent(), Author2.Represent(), Album_s.Represent());

        }*/


}
    class Program
    {
        static void Main(string[] args)
        {
            /*Compositor HU = new Compositor("Hollywood", "Undead");
            Compositor PR = new Compositor("Papa", "Roach");
            Compositor INK = new Compositor("ICE NINE", "KILLS");
            Compositor XX = new Compositor("The", "XX");
            Compositor Bach = new Compositor("Иоган", "Бах");
            Album NE2 = new Album("New Empire,Vol.2",2020);
            Song HOTC = new  Song("Heart of the Champion","Rock",2020,HU, PR, INK,NE2);
            Song CR = new Song("Crystailsed","Indie", 2018, XX);*/
            Compositor LDR = new Compositor
            {
                Name = "Lana",
                Surname = "Del Rey"

            };
            Compositor WD = new Compositor
            {
                Name = "Weekend",
                Surname = " "

            };
            Album LFLA = new Album
            {
                Name = "Lust for Life",
                Year = 2017,
            };
            Song LFL = new Song
            {
                Name = "Lust for Life",
                Year = 2017,
                Gener = "Indie",
                Authors = new Compositor[] { LDR, WD },
                Album_s = LFLA

            };


            string json = JsonSerializer.Serialize<Song>(LFL);
            Console.WriteLine(json);
            Console.ReadKey();
        }
    }
}
